module.exports = function(dal){
  var authHelper = new Object();
   authHelper.isLoggedIn = function(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
    return next();
    // if they aren't redirect them to the home page
    res.redirect('/login');
  }

  authHelper.isAdmin = function(req,res,next){
    if(req.isAuthenticated())
      dal.rol.GetByUserId(req.session.passport.user,function(err,rows){
        if(err)
          console.log(err);
        else
          if (rows[0].rol===1)
            return next();
          else
            res.redirect("/my-account")
      });
    else
      res.redirect('/login');
  }

  authHelper.isMember = function(req,res,next){
    if(req.isAuthenticated())
      dal.rol.GetByUserId(req.session.passport.user, function(err,rows){
        if(err)
          console.log(err);
        else
          if (rows[0].rol<=2)
            return next();
          else
            res.redirect("/my-account")
      });
    else
      res.redirect('/login');
  }

  return authHelper;
}
