module.exports = function(pool){
    var rol = new Object();

    rol.GetAll = function(callback){
      pool.query("SELECT * FROM rol",function(err,rows){
        callback(err,rows);
      });
    }

    rol.GetByUserId = function(UserId, callback){
      pool.query("SELECT rol FROM users WHERE ID = ?",[UserId],function(err, rows){
        callback(err, rows);
      });
    }

    return rol;
}
