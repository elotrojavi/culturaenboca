var async = require('async');

module.exports = function(app, dal, passport){

  var authHelper = require('./authenticationHelper.js')(dal);
  app.get('/login', function(req, res) {
    res.render('accounts/login', {errors: req.flash('loginMessage'), successes: req.flash('correct')});
  });

  app.post('/login', passport.authenticate('local-login', {
    failureRedirect : '/login',
    failureFlash : true
  }),
  function(req, res) {
    if (req.body.remember) {
      req.session.cookie.maxAge = 1000 * 60 * 60 * 24* 30;
    } else {
      req.session.cookie.expires = false;
    }
    res.redirect('/my-account');
  });

  app.get('/logout', authHelper.isLoggedIn, function(req, res){
    req.logout();
    res.redirect("/");
  });

  // app.get('/forgotpassword', function(req, res){
  //   res.render('accounts/forgotpassword', {message: req.flash('databaseError')})
  // });
  //
  // app.post('/forgotpassword', function(req, res){
  //   async.waterfall([
  //     function(callback){
  //       pool.query("SELECT ID, email FROM users WHERE email= ?", [req.body.email], function(err, rows){
  //         callback(err, rows);
  //       });
  //     },
  //     function(user, callback){
  //       if(user.length > 0){
  //         var gid;
  //         do{
  //           gid = guid.raw();
  //           gid= bcrypt.hashSync(gid, bcrypt.genSaltSync(10));
  //         } while(gid.includes('/'));
  //         var timeStamp;
  //         var userID = user[0].ID;
  //         callback(null, gid, timeStamp, userID);
  //       } else
  //         callback('No user found');
  //     },
  //     function(gid, timeStamp, userID, callback){
  //       pool.query('INSERT INTO password_change_request (ID, uID, time) VALUES (?,?,?)', [gid, userID, timeStamp], function(err, rows){
  //         callback(err, gid);
  //       });
  //     }
  //   ],function(err, results){
  //     if(err){
  //       req.flash('databaseError', 'Ha habido un error, Por favor intentelo de nuevo');
  //       res.redirect("/forgotpassword");
  //     }
  //     else
  //     nodemailer.createTestAccount((err, account) => {
  //
  //       // create reusable transporter object using the default SMTP transport
  //       let transporter = nodemailer.createTransport({
  //         host: 'smtp.ethereal.email',
  //         port: 587,
  //         secure: false, // true for 465, false for other ports
  //         auth: {
  //           user: account.user, // generated ethereal user
  //           pass: account.pass  // generated ethereal password
  //         }
  //       });
  //
  //       // setup email data with unicode symbols
  //       let mailOptions = {
  //         from: '"Culturaenboca" <donotreply@Culturaenboca.com>', // sender address
  //         to: req.body.email, // list of receivers
  //         subject: 'Recuperar contraseña', // Subject line
  //         text: '192.168.0.14:3000/resetpassword/'+results, // plain text body
  //         html: '<a href="http://192.168.0.14:3000/resetpassword/'+results+'">Cambiar contraseña</a>' // html body
  //       };
  //
  //       // send mail with defined transport object
  //       transporter.sendMail(mailOptions, (error, info) => {
  //         if (error) {
  //           return console.log(error);
  //         }
  //         console.log('Message sent: %s', info.messageId);
  //         // Preview only available when sending through an Ethereal account
  //         console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
  //         res.redirect("/");
  //         // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@blurdybloop.com>
  //         // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
  //       });
  //     });
  //   });
  // });
  //
  // app.get('/resetpassword/:id', function(req, res){
  //   pool.query("SELECT ID FROM password_change_request WHERE ID = ?", [req.params.id], function(err, rows){
  //     if(err)
  //       console.log(err);
  //     else{
  //       if(rows.length){
  //         res.render('accounts/resetpassword', {token: rows[0]});
  //       } else {
  //         res.redirect('/');
  //       }
  //     }
  //   });
  // });
  //
  // app.post('/resetpassword/:id', function(req, res){
  //   async.waterfall([
  //     function(callback){
  //       pool.query("SELECT * FROM password_change_request WHERE ID = ?", [req.params.id], function(err, rows){
  //         callback(err, rows);
  //       });
  //     },
  //     function(token, callback){
  //       if(token){
  //         var id = token[0].uID;
  //         password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
  //         token = token[0].ID;
  //         callback (null, id, password, token);
  //       } else
  //         callback("No token found");
  //     },
  //     function(id, password, token, callback){
  //       pool.query("UPDATE users SET password = ? WHERE ID = ?", [password, id], function(err,rows){
  //         callback(err, token);
  //       });
  //     },
  //     function(token, callback){
  //       pool.query("DELETE FROM password_change_request WHERE ID = ?",[token],function(err, rows){
  //         callback(err,rows);
  //       });
  //     }
  //   ], function(err, results){
  //     if(err){
  //       req.flash('databaseError', 'Ha habido un error, por favor intentelo de nuevo');
  //       res.redirect("/forgotpassword");
  //     }else
  //       res.redirect("/");
  //   });
  // });
}
