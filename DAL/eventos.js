module.exports = function(pool){
    var eventos = new Object();

    eventos.GetAll = function(callback){
      pool.query("SELECT e.titulo, e.info, e.link, e.fecha, e.lugar, e.precio, u.color, i.ruta, i.texto_alternativo FROM eventos e, users u, imagenes i WHERE e.uID = u.ID AND e.publicado = 1 AND e.portada = i.ID ORDER BY fecha_int ASC", function(err,rows){
        callback(err, rows);
      });
    };

    eventos.GetById = function(Id, callback){
      pool.query("SELECT ID, titulo, info, link, fecha, lugar, precio, fecha_int, portada FROM eventos WHERE ID = ?",[Id], function(err, rows){
        callback(err, rows);
      });
    }
    eventos.GetByUserId = function(userId, callback){
      pool.query('SELECT e.ID, e.titulo, e.info, e.link, e.fecha, e.publicado, i.ruta, i.texto_alternativo FROM eventos e, imagenes i WHERE e.uID=? AND e.portada = i.ID ORDER BY e.fecha_int DESC',[userId],function(err, rows){
        callback(err,rows);
      });
    };

    eventos.GetByIdAndUserId = function(Id, userId, callback){
      pool.query("SELECT u.ID FROM users u, eventos e WHERE e.ID = ? AND e.uID = ?",[Id, userId],function(err,rows){
        callback(err, rows);
      });
    }

    eventos.Add = function(evento, callback){
      pool.query('INSERT INTO eventos (titulo,info,link,uId,publicado,fecha,lugar, precio,fecha_int, portada) VALUES (?,?,?,?,?,?,?,?,?,?)', [evento.titulo, evento.info, evento.link, evento.autor, evento.publish, evento.fecha, evento.lugar, evento.precio, evento.fecha_int, evento.portada],function(err,rows){
        callback(err, rows);
      });
    };

    eventos.Publish = function(Id, publish, callback){
      pool.query("UPDATE eventos SET publicado = ? WHERE ID = ? ",[publish, Id], function(err, rows){
        callback(err, rows);
      });
    };

    eventos.UpdateById = function(Id, evento, callback){
      pool.query("UPDATE eventos SET titulo = ?, info = ?, link = ?, fecha = ?, lugar = ?, precio = ?, fecha_int = ?, portada = ? WHERE ID = ?", [evento.titulo, evento.info, evento.link, evento.fecha, evento.lugar, evento.precio, evento.fecha_int, evento.portada,Id], function(err, rows){
        callback(err, rows);
      })
    }

    eventos.DeleteById = function(Id, callback){
      pool.query("DELETE FROM eventos WHERE ID = ?",[Id],function(err, rows){
        callback(err, rows);
      });
    };

    return eventos;
}
