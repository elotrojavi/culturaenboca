var async = require('async');

module.exports = function(app, dal, passport){

  var authHelper = require('../authenticationHelper.js')(dal);

  app.get('/my-account/proyectartes', authHelper.isMember, function(req,res){
    async.parallel({
      rol:function(callback){
        dal.rol.GetByUserId(req.session.passport.user, callback);
      },
      proyectartes:function(callback){
        dal.proyectos.GetByUserId(req.session.passport.user, 1, callback);
      }
    }, function(err, results){
      if(err){
        req.flash('error','Ha habido un error, por favor intentelo de nuevo');
        console.log(err);
        res.redirect("/my-account")
      } else {
        res.render("profile/proyectarte/index", {proyectartes:results.proyectartes, rol:results.rol[0].rol, errors:req.flash('error'), successes:req.flash('correct')});
      }
    });
  });

  app.get("/my-account/proyectartes/new", authHelper.isMember,function(req,res){
    dal.users.GetDistinct(req.session.passport.user, function(err,rows){
      if(err){
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        res.redirect("/my-account/proyectartes");
      }
      else{
        res.render("profile/proyectarte/new", {users:rows});
      }
    });
  });

  app.post("/my-account/proyectartes", authHelper.isMember, function(req,res){
    var articulo = setProyectarte(req.body.impactarte, req.session.passport.user);
    dal.proyectos.Add(articulo,function(err, results){
        if (err){
          console.log(err);
          req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        }
        else
          req.flash('correct', 'Se ha creado el proyectarte correctamente');
        res.redirect("/my-account/proyectartes");
      });
  });

  app.get("/my-account/proyectartes/:id", authHelper.isMember, function(req,res){
    async.parallel({
      articulo: function(callback){
        dal.proyectos.GetByIdAndUserId(req.params.id, req.session.passport.user, callback);
      },
      autores: function(callback){
        dal.users.GetByProyectosID(req.params.id, callback);
      }
    },function(err, results){
      if(err || !results.articulo || !results.autores || results.articulo.length==0 || results.autores.length==0){
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        console.log(err);
        res.redirect("/my-account/proyectartes");
      }
      else{
        results.articulo[0].autores = results.autores;
        var color = "#000000";
        if(results.autores.length===1)
          color = results.autores[0].color;
        res.render("profile/proyectarte/show",{articulo:results.articulo[0], color:color});
      }
    });
  });

  app.delete("/my-account/proyectartes/:id", authHelper.isMember, function(req,res){
    async.series([
      function(callback){
        dal.proyectos.GetByIdAndUserId(req.params.id, req.session.passport.user,function(err, rows){
          if(err)
            callback(err, rows);
          else
            if (rows.length===0)
              callback("Este proyectarte no te pertenece", rows);
            else
              callback(err, rows);
        });
      },
      function(callback){
        dal.proyectos.DeleteById(req.params.id, callback);
      }
    ],function(err, results){
      if(err)
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
      else
        req.flash('correct', 'Se ha eliminado el proyectarte correctamente');
      res.redirect("/my-account/proyectartes");
    });
  });

  app.get("/my-account/proyectartes/edit/:id", authHelper.isMember, function(req,res){
    async.parallel({
      totUser : function(callback){
        dal.users.GetDistinct(req.session.passport.user, callback);
      },
      artAut : function(callback){
        dal.users.GetByProyectosID(req.params.id, callback);
      },
      articulo : function(callback){
        dal.proyectos.GetByIdAndUserId(req.params.id, req.session.passport.user, callback);
      }
    }, function(err, results){
      if(err || results.articulo.length == 0){
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        console.log(err);
        res.redirect("/my-account/impactartes");
      }
      else{
        var artAutores = new Array();
        results.artAut.forEach(function(autor){artAutores.push(autor.ID)});
        res.render("profile/proyectarte/edit",{users: results.totUser, artAutores: artAutores, art:results.articulo[0]});
      }
    });
  });

  app.put("/my-account/proyectartes/:id", authHelper.isMember, function(req,res){
    async.series([
      function(callback){
        dal.proyectos.GetByIdAndUserId(req.params.id, req.session.passport.user, function(err, rows){
          if(err)
            callback(err, null);
          else
            if (rows.length===0)
              callback("Este articulo no te pertenece", null);
            else
              callback(null, "ONE");
        });
      },
      function(callback){
        if(req.body.publish)
          dal.proyectos.Publish(req.params.id, req.body.publish, callback);
        else{
            var proyectarte = setProyectarte(req.body.articulo, req.session.passport.user);
            proyectarte.id = req.params.id;
            dal.proyectos.Update(proyectarte, callback);
          }
      }
    ], function(err, result){
      if(err){
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        console.log(err);
      }
      else
        req.flash('correct', 'Se ha modificado el proyectarte correctamente');
      res.redirect("/my-account/proyectartes");
    });
  });
}

function setProyectarte(proyectarte, userId){
  if(proyectarte.descripcion=="")
    proyectarte.descripcion=null;
  var fecha = new Date();
  proyectarte.fecha = fecha;
  proyectarte.tipo = 1;
  proyectarte.users = setAutores(proyectarte.users, userId)
  return proyectarte;
}

function setAutores(users, userId){
  var autoresNew = new Array();
  if(users){
    if(typeof(users) !='string')
      for(var i=0;i<users.length;i++){autoresNew.push(Number(users[i]));}
    else
      autoresNew.push(Number(users));
  }
  autoresNew.push(userId);
  return autoresNew;
}
