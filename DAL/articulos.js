var async                 = require('async'),
autores_articulos     = require("./autores_articulos.js")(),
articulos_categoria   = require("./articulos_categoria.js")();

module.exports = function(pool){
  var articulos = new Object();

  articulos.GetAll = function(callback){
    pool.query('SELECT a.ID, a.titulo, a.descripcion, i.ruta, i.texto_alternativo ,GROUP_CONCAT(u.name," ",u.surname SEPARATOR ", ") as autores FROM articulos a, users u, autores_articulos aa, imagenes i WHERE a.publicado=1 AND u.ID = aa.uID AND a.ID = aa.aID AND a.portada = i.ID GROUP BY a.ID ORDER BY a.fecha_publicacion DESC', function (err, rows) {
      callback(err, rows);
    });
  }

  articulos.GetRecientes = function(callback){
    pool.query('SELECT a.ID, a.titulo, a.descripcion, i.ruta, i.texto_alternativo ,GROUP_CONCAT(u.name," ",u.surname SEPARATOR ", ") as autores FROM articulos a, users u, autores_articulos aa, imagenes i WHERE a.publicado=1 AND u.ID = aa.uID AND a.ID = aa.aID AND a.portada = i.ID GROUP BY a.ID ORDER BY a.fecha_publicacion DESC LIMIT 7', function(err,rows){
      callback(err,rows);
    });
  }

  articulos.GetByCategoria = function(categoria, callback){
    pool.query('SELECT a.ID, a.titulo, a.descripcion, GROUP_CONCAT(u.name," ",u.surname SEPARATOR ", ") as autores, i.ruta, i.texto_alternativo FROM articulos a, articulos_categoria ac, users u, autores_articulos aa, imagenes i WHERE ac.cID = ? AND a.publicado = 1 AND ac.aID = a.ID AND u.ID = aa.uID AND a.ID = aa.aID AND a.portada = i.ID GROUP BY a.ID ORDER BY a.fecha_publicacion DESC', [categoria], function(err, rows){
      callback(err, rows);
    });
  }

  articulos.GetById = function(Id, callback){
    pool.query("SELECT a.ID, a.titulo, a.subtitulo , a.descripcion, a.articulo, i.ruta FROM articulos a, imagenes i WHERE a.ID = ? AND a.portada = i.ID", [Id], function(err, rows){
      callback(err, rows);
    });
  }

  articulos.GetByUserId = function(Id, callback){
    pool.query("SELECT a.ID, a.titulo, a.descripcion, a.publicado, i.ruta FROM articulos a, autores_articulos aa, imagenes i WHERE a.ID = aa.aID AND aa.uID = ? AND a.portada = i.ID ORDER BY fecha_publicacion DESC",[Id], function(err, rows){
      callback(err,rows);
    });
  }

  articulos.GetPublishedByUserId = function(Id, callback){
    pool.query('SELECT a.ID, a.titulo, a.descripcion, i.ruta, i.texto_alternativo ,GROUP_CONCAT(u.name," ",u.surname SEPARATOR ", ") as autores FROM articulos a, users u, autores_articulos aa, imagenes i WHERE a.publicado=1 AND u.ID = aa.uID AND a.ID = aa.aID AND aa.uID = ? AND a.portada = i.ID GROUP BY a.ID ORDER BY a.fecha_publicacion DESC LIMIT 5', [Id], function(err,rows){
      callback(err,rows);
    });
  }

  articulos.GetByTitulo = function(titulo, callback){
    pool.query("SELECT a.ID, a.titulo, a.subtitulo , a.descripcion, a.articulo, i.ruta FROM articulos a, imagenes i WHERE a.titulo = ? AND a.publicado = 1 AND a.portada = i.ID", [titulo], function(err, rows){
      callback(err, rows);
    });
  }

  articulos.GetByTituloAndCategoria = function(titulo, categoria, callback){
    pool.query("SELECT a.ID, a.titulo, a.subtitulo ,a.articulo,a.descripcion, i.ruta FROM articulos a, articulos_categoria ac, imagenes i WHERE a.titulo = ? AND a.publicado = 1 AND ac.cID = ? AND a.ID = ac.aID AND a.portada = i.ID", [titulo, categoria], function(err, rows){
      callback(err, rows);
    });
  }

  articulos.GetByIdAndUserId = function(Id, userId, callback){
    pool.query("SELECT a.ID, a.titulo, a.subtitulo ,a.articulo ,a.descripcion,a.portada, i.ruta FROM articulos a, autores_articulos aa, imagenes i WHERE a.ID = ? AND aa.uID = ? AND a.ID = aa.aID AND a.portada = i.ID", [Id, userId], function(err, rows){
      callback(err, rows);
    });
  }

  articulos.Insert = function(articulo, connection, callback){
    connection.query("INSERT INTO articulos (titulo, subtitulo, articulo, fecha_publicacion, publicado, portada, descripcion) VALUES (?,?,?,?,?,?,?)",[articulo.titulo, articulo.subtitulo, articulo.articulo, articulo.fecha, articulo.publish, articulo.portada, articulo.descripcion], function(err, rows){
      callback(err, rows);
    });
  }

  articulos.Add = function(articulo, callback){
    pool.getConnection(function(err, connection){
      async.series([
        function(asyncCallback){
          connection.beginTransaction(function(err){
            asyncCallback(err, "TRANSACTION");
          });
        },
        function(asyncCallback){
          AddProcess(articulo, connection, asyncCallback);
        },
        function(asyncCallback){
          connection.commit(function(err){
            asyncCallback(err, "COMMIT");
          });
        }
      ], function(err, results){
        if (err)
        connection.rollback();
        connection.release();
        callback(err, results);
      });
    });
  }

  articulos.Publish = function(Id, publish, callback){
    pool.query("UPDATE articulos SET publicado = ?, fecha_publicacion = ? WHERE ID = ? ",[publish,new Date(), Id], function(err, rows){
      callback(err, rows);
    });
  }

  articulos.Modify = function(articulo, connection, callback){
    connection.query("UPDATE articulos SET titulo = ?, subtitulo = ?, articulo = ?, portada = ?, descripcion= ? WHERE ID = ?",[articulo.titulo, articulo.subtitulo, articulo.articulo, articulo.portada,articulo.descripcion,articulo.id], function(err,rows){
      callback(err, rows);
    });
  }

  articulos.Update = function(articulo, callback){
    pool.getConnection(function(err, connection){
      async.series([
        function(asyncCallback){
          connection.beginTransaction(function(err){
            asyncCallback(err, "TRANSACTION");
          });
        },
        function(asyncCallback){
          UpdateProcess(articulo, connection, asyncCallback);
        },
        function(asyncCallback){
          connection.commit(function(err){
            asyncCallback(err, "COMMIT");
          });
        }
      ], function(err, results){
        if (err)
        connection.rollback();
        connection.release();
        callback(err, results);
      });
    });
  }

  articulos.DeleteById = function(Id, callback){
    pool.query("DELETE FROM articulos WHERE ID = ?",[Id],function(err, rows){
      callback(err, rows);
    });
  }

  //Métodos auxiliares
  function AddProcess(articulo, connection, callback){
    async.waterfall([
      function(asyncCallback){
        articulos.Insert(articulo, connection, asyncCallback);
      },
      function(article, asyncCallback){
        autores_articulos.AddBulk(articulo.users, article.insertId, connection, function(err, rows){
          asyncCallback(err, article);
        });
      },
      function(article, asyncCallback){
        articulos_categoria.AddBulk(articulo.categorias, article.insertId, connection, asyncCallback);
      }
    ],function(err, result){
      callback(err, "ADDPROCESS");
    });
  }

  function UpdateProcess(articulo, connection, callback){
    async.parallel([
      function(asyncCallback){
        articulos.Modify(articulo, connection, asyncCallback);
      },
      function(asyncCallback){
        UpdateAuthorsProcess(articulo.id, articulo.users, connection, asyncCallback);
      },
      function(asyncCallback){
        UpdateCategoriasProcess(articulo.id, articulo.categorias, connection, asyncCallback)
      }
    ], function(err, results){
      callback(err, results);
    });
  }

  function UpdateAuthorsProcess(Id, users, connection, callback){
    async.waterfall([
      function(asyncCallback){
        autores_articulos.GetByArticulosID(Id, connection, asyncCallback);
      },
      function(autoresBBDD, asyncCallback){
        var autoresOld = new Array();
        autoresBBDD.forEach(function(autor){autoresOld.push(autor.uID)});
        var autoresAnadir = Distinct(users, autoresOld);
        var autoresBorrar = Distinct(autoresOld, users);
        UpdateAuthors(Id, autoresAnadir, autoresBorrar, connection, asyncCallback);
      }
    ],function(err ,results){
      callback(err,results);
    });
  }

  function UpdateAuthors(Id, addedAuthors, deletedAuthors, connection, callback){
    async.parallel([
      function(asyncCallback){
        autores_articulos.AddBulk(addedAuthors, Id, connection, asyncCallback);
      },
      function(asyncCallback){
        autores_articulos.DeleteBulk(deletedAuthors, Id, connection, asyncCallback);
      }
    ],function(err, results){
      callback(err, results);
    });
  }

  function UpdateCategoriasProcess(Id, categorias, connection, callback){
    async.waterfall([
      function(asyncCallback){
        articulos_categoria.GetByArticulosID(Id, connection, asyncCallback);
      },
      function(categoriasBBDD, asyncCallback){
        var categoriasOld = new Array();
        categoriasBBDD.forEach(function(categoria){categoriasOld.push(categoria.cID)});
        var categoriasAnadir = Distinct(categorias, categoriasOld);
        var categoriasBorrar = Distinct(categoriasOld,categorias);
        UpdateCategorias(Id, categoriasAnadir, categoriasBorrar, connection, asyncCallback);
      },
    ],function(err ,results){
      callback(err,results);
    });
  }


  function UpdateCategorias(Id, addedCategorias, deletedCategorias, connection, callback){
    async.parallel([
      function(asyncCallback){
        articulos_categoria.AddBulk(addedCategorias, Id, connection, asyncCallback);
      },
      function(asyncCallback){
        articulos_categoria.DeleteBulk(deletedCategorias, Id, connection, asyncCallback);
      }
    ],function(err, results){
      callback(err, results);
    });
  }

  function Distinct(New, Old){
    var result = new Array();
    New.forEach(function(element){
      if(!Old.includes(element))
      result.push(element);
    });
    return result;
  }

  return articulos;
}
