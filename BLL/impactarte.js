var async = require('async');

module.exports = function(app, dal, passport){

  app.get('/impactartes', function(req,res){
    dal.proyectos.GetPublished(0, function(err, rows){
      if(err){
        console.log(err);
        res.redirect("/");
      } else {
        res.render("proyectos/impactartes", {impactartes:rows}) ;
      }
    });
  });

  app.get('/impactartes/:pID', function(req, res){
    titulo = decodeURIComponent(req.params.pID).split("_").join(" ");
    async.waterfall([
      function(callback){
        dal.proyectos.GetByTitle(titulo, callback);
      },
      function(impactarte, callback){
        dal.users.GetByProyectosID(impactarte[0].ID, function(err, rows){
          if(!err){
            impactarte[0].autores=rows;
          }
          callback(err, impactarte)
        });
      }
    ], function(err,results){
      if(err){
        console.log(err);
        res.redirect("/impactartes");
      }
      var color= "#000000";
      if(results[0].autores.length===1)
        color = results[0].autores[0].color;
      res.render("proyectos/impactarte",{impactarte:results[0], color:color});
    });
  });
}
