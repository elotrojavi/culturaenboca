var form = document.getElementById('registro');
var email = document.getElementsByName('email')[0];
var password = document.getElementsByName('password')[0];
var passwordConfirm = document.getElementsByName('password-confirm')[0];
var is_update = false;

if(form==undefined)
  form = document.getElementById('resetpassword');
if(form==undefined){
  form = document.getElementById('updateUser');
  is_update =true
}

if(email !== undefined){
  email.addEventListener("input", function(event){
    if(email.validity.valid){
      email.classList.add("input-correct");
      email.classList.remove("input-error");
    } else {
      email.classList.remove("input-correct");
      email.classList.add("input-error");
    }
  });
}


passwordConfirm.addEventListener("input", function(event){
  if(password.value!==passwordConfirm.value){
      passwordConfirm.classList.add("input-error");
      password.classList.add("input-error");
      passwordConfirm.classList.remove("input-correct");
      password.classList.remove("input-correct");
    } else if (!is_update && password.value!=="") {
      passwordConfirm.classList.remove("input-error");
      password.classList.remove("input-error");
      passwordConfirm.classList.add("input-correct");
      password.classList.add("input-correct");
    } else if (is_update){
      passwordConfirm.classList.remove("input-error");
      password.classList.remove("input-error");
      passwordConfirm.classList.add("input-correct");
      password.classList.add("input-correct");
    }
});

password.addEventListener("input", function(event){
  if(password.value!==passwordConfirm.value){
      passwordConfirm.classList.add("input-error");
      password.classList.add("input-error");
      passwordConfirm.classList.remove("input-correct");
      password.classList.remove("input-correct");
    } else if (!is_update && password.value!=="") {
      passwordConfirm.classList.remove("input-error");
      password.classList.remove("input-error");
      passwordConfirm.classList.add("input-correct");
      password.classList.add("input-correct");
    } else if (is_update){
      passwordConfirm.classList.remove("input-error");
      password.classList.remove("input-error");
      passwordConfirm.classList.add("input-correct");
      password.classList.add("input-correct");
    }
});


form.addEventListener("submit", function(event){
  if(password.value!==passwordConfirm.value){
    alert('Passwords do not match');
    event.preventDefault();
  }
});
