ncarousel = 1;
document.execCommand("defaultParagraphSeparator", false, "p");
 $('.editorToolbar button').click(function(e) {
   var command = $(this).data('command');
   if (command == 'createlink') {
     url = prompt('Introduce el link aquí: ', 'http:\/\/');
     document.execCommand($(this).data('command'), false, url);
   } else if (command){
     document.execCommand($(this).data('command'), false, null);
   }
   var editor = document.getElementById('contenido');
   editor.focus();
 });

 $('#imagen').click(function(e){
   $.ajax("/api/imagenes", {success: function(result){
     imagenes="";
     modal = $("#seleccionarImagen .modal-body .card-columns");
     result.forEach(function(imagen){
       imagenes+='<div class="card materias">';
       imagenes+="<label>";
       imagenes+="<input type='radio' name='imagen' value='"+imagen.ID+"'>";
       imagenes+='<img src="'+imagen.ruta+'" alt="'+imagen.texto_alternativo+'" class= "card-img">';
       imagenes+='</label>';
       imagenes+='</div>';
     });
     modal.html(imagenes);
   }});
 });

 $('#gallery').click(function(e){
   $.ajax("/api/imagenes", {success: function(result){
     imagenes="";
     modal = $("#seleccionarGaleria .modal-body .card-columns");
     result.forEach(function(imagen){
       imagenes+='<div class="card materias">';
       imagenes+="<label>";
       imagenes+="<input type='checkbox' name='gall' value='"+imagen.ID+"'>";
       imagenes+='<img src="'+imagen.ruta+'" alt="'+imagen.texto_alternativo+'" class= "card-img">';
       imagenes+='</label>';
       imagenes+='</div>';
     });
     modal.html(imagenes);
   }});
 });

 $('#portadaBtn').click(function(e){
   $.ajax("/api/imagenes", {success: function(result){
     imagenes="";
     modal = $("#seleccionarPortada .modal-body .card-columns");
     result.forEach(function(imagen){
       imagenes+='<div class="card materias">';
       imagenes+="<label>";
       imagenes+="<input type='radio' name='portadaList' value='"+imagen.ID+"'>";
       imagenes+='<img src="'+imagen.ruta+'" alt="'+imagen.texto_alternativo+'" class= "card-img">';
       imagenes+='</label>';
       imagenes+='</div>';
     });
     modal.html(imagenes);
   }});
 });

 $("#anadirImgPerfil").click(function(e){
   imgID = $("input[type='radio'][name='portadaList']:checked").val();
   $("#img").val(imgID)
   ruta = "/api/imagenes/"+imgID
   $.ajax(ruta, {success: function(result){
     $("#imgPerfil").attr("src",result.ruta);
     $("#imgPerfil").attr("alt",result.texto_alternativo);
   }});
 });

 $("#anadirPortada").click(function(e){
   imgID = $("input[type='radio'][name='portadaList']:checked").val();
   $("#portada").val(imgID);
 });

 $("#anadirImg").click(function(e){
   imgID = $("input[type='radio'][name='imagen']:checked").val();
   ruta = "/api/imagenes/"+imgID
   $.ajax(ruta, {success: function(result){
     html = "<img src='"+result.ruta+"' alt='"+result.texto_alternativo+"' class='img-fluid'>"
     document.execCommand("insertHTML", false, html);
   }});
 });

 $("#anadirGallery").click(function(e){
   first = true;
   html ="<div id='carousel1' class='carousel slide' data-ride='carousel'><div class='carousel-inner'>";
   img = $("input[type='checkbox'][name='gall']:checked");
   console.log(img);
   img.each(function(i){
     $.ajax("/api/imagenes/"+$(this).val(), {async:false, success:function(result){
       html+="<div class='carousel-item";
       if(first){
        html+=" active";
        first=false;
       }
       html+="'>"
       html+="<img class='d-block w-100' src='"+result.ruta+"' alt='"+result.texto_alternativo+"'>";
       html+="</div>"
     }});
   });
   html+="</div>";
   html+='<a class="carousel-control-prev" href="#carousel1" role="button" data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="carousel-control-next" href="#carousel1" role="button" data-slide="next"><span class="carousel-control-next-icon" aria-hidden="true"></span><span class="sr-only">Next</span></a></div>';
   console.log(html);
   document.execCommand("insertHTML", false, html);
 });

 $('#encabezados').change(function(e) {
   var command = $("#encabezados option:selected").data('command');
   if (command == 'h1' || command == 'h2' || command == 'h3' || command == 'h4' || command == 'h5' || command == 'h6' || command == 'P') {
     document.execCommand('formatBlock', false, command);
   }
   document.execCommand(command, false, null);
 });

 $(".chosen-select").chosen();

 $("#save").click(function(e){
   if(validate())
    sendArticle(0);
 });
 $("#publishB").click(function(e){
   if(validate())
    sendArticle(1);
 });


$('#latex').click(function(e){
  console.log(window.getSelection());
  selection = "<span class='latex'>"+window.getSelection();+"</span>"
  document.execCommand('insertHTML',false,selection);
});

$('#insertHTML').click(function(e){
  html = prompt("Introduce el html aquí");
  html = "<div class='row justify-content-center'><div class='col-12 col-sm-8'><div class='video-container'>"+html+"</div></div></div>"
  document.execCommand("insertHTML", false, html);
});

function sendArticle(publish){
  $("#articulo").val($("#contenido").html());
  $("#publish").val(publish);
  $("#editor").submit();
}

function validate(){
  valid = false;
  accion = $("#editor").attr('action');
  if(accion.includes("articulos"))
    valid = validateArticulo();
  else if (accion.includes("eventos"))
    valid = validateEvento();
  else if (accion.includes("impactartes")||accion.includes("proyectartes"))
    valid = validateProyecto();
  return valid
}

function validateArticulo(){
  valid = true;
  errorList = "";
  titulo = document.getElementsByName("articulo[titulo]")[0];
  if(titulo.value.length>255){
    errorList +="<li>El título es demasiado largo, el tamaño máximo son 255 caracteres, y el título actual tiene: "+titulo.value.length+"</li>";
    valid=false;
  } else if (titulo.value.length==0){
    errorList += "<li>El titulo no puede estar vacío</li>"
    valid = false;
  }
  subtitulo = document.getElementsByName("articulo[subtitulo]")[0];
  if(subtitulo)
    if(subtitulo.value.length>=400){
      errorList += "<li>El subtítulo es demasiado largo, el tamaño máximo son 400 caracteres, y el subtítulo actual tiene: "+subtitulo.value.length+"</li>";
      valid=false;
    }
  categorias = $("#categorias")[0];
  if(categorias){
    if(categorias.value==""){
      errorList += "<li>Es necesario asignar, al menos, una categoría</li>";
      valid=false;
    }
  }
  contenido = $("#contenido")[0];
  if(contenido.innerHTML.length>65535){
      errorList += "<li>El artículo es demasiado largo, el tamaño máximo son 65.535 caracteres, y el artículo actual tiene: "+contenido.innerHTML.length+"</li>";
    valid = false;
  }else if(contenido.innerHTML.length==0){
    errorList += "<li>El artículo no puede estar vacío</li>";
    valid = false;
  }
  descripcion=document.getElementsByName("articulo[descripcion]")[0];
  if(descripcion.value.length>600){
    errorList += "<li>La descripción es demasiado larga, el tamaño máximo son 600 caracteres, y el artículo actual tiene: "+descripcion.value.length+"</li>";
    valid=false;
  }
  else if (descripcion.value.length==0){
    errorList += "<li>La descripción no puede estar vacía</li>";
    valid = false;
  }
  if(!valid)
    showWarning(errorList);
  return valid;
}

function validateEvento(){
  valid = true
  errorList = "";
  titulo = document.getElementsByName("evento[titulo]")[0];
  if(titulo.value.length>255){
    errorList +="<li>El título es demasiado largo, el tamaño máximo son 255 caracteres, y el título actual tiene: "+titulo.value.length+"</li>";
    valid=false;
  } else if (titulo.value.length==0){
    errorList += "<li>El titulo no puede estar vacío</li>"
    valid = false;
  }
  fechaMostrar = document.getElementsByName("evento[fecha]")[0];
  if(fechaMostrar.value.length>60){
        errorList +="<li>La fecha para mostrar es demasiado larga, el tamaño máximo son 60 caracteres, y la fecha actual tiene: "+fechaMostrar.value.length+"</li>";
    valid = false;
  } else if (fechaMostrar.value.length==0){
    errorList += "<li>La fecha para mostrar no puede estar vacía</li>"
    valid= false;
  }

  fecha = document.getElementsByName("evento[fecha_int]")[0];
  if(fecha.value.length==0){
    errorList+= "<li>Debes asignar una fecha al evento</li>";
    valid = false;
  }
  link = document.getElementsByName("evento[link]")[0];
  if(link.value.length>255){
        errorList +="<li>El link es demasiado largo, el tamaño máximo son 255 caracteres, y el link actual tiene: "+link.value.length+"</li>";
    valid = false;
  }

  lugar = document.getElementsByName("evento[lugar]")[0];
    if(lugar.value.length>140){
      errorList +="<li>El lugar es demasiado largo, el tamaño máximo son 140 caracteres, y el lugar actual tiene: "+lugar.value.length+"</li>";
      valid = false;
    } else if (lugar.value.length == 0){
        errorList += "<li>El lugar no puede estar vacío</li>";
      valid = false;
    }
    precio = document.getElementsByName("evento[precio]")[0];
      if(precio.value.length>80){
      errorList +="<li>El precio es demasiado largo, el tamaño máximo son 80 caracteres, y el precio actual tiene: "+precio.value.length+"</li>";
        valid = false;
      } else if (precio.value.length == 0){
        errorList += "<li>El precio no puede estar vacío</li>";
        valid = false;
      }
      contenido = $("#contenido")[0];
      if(contenido.innerHTML.length>700){
      errorList +="<li>La información del evento es demasiado larga, el tamaño máximo son 700 caracteres, y la información actual tiene: "+contenido.innerHTML.length+"</li>";
        valid = false;
      }else if(contenido.innerHTML.length==0){
        errorList += "<li>La información del evento no puede estar vacía</li>";
        valid = false;
      }
      if(!valid)
        showWarning(errorList);
      return valid;
}

function validateProyecto(){
  valid = true
  errorList = "";
  titulo = document.getElementsByName("impactarte[titulo]")[0];
  if (titulo==undefined)
    titulo= document.getElementsByName("articulo[titulo]")[0];
    if(titulo.value.length>255){
      errorList +="<li>El título es demasiado largo, el tamaño máximo son 255 caracteres, y el título actual tiene: "+titulo.value.length+"</li>";
      valid=false;
    } else if (titulo.value.length==0){
      errorList += "<li>El titulo no puede estar vacío</li>"
      valid = false;
    }
  contenido = $("#contenido")[0];
  if(contenido.innerHTML.length>65535){
      errorList += "<li>El artículo es demasiado largo, el tamaño máximo son 65.535 caracteres, y el artículo actual tiene: "+contenido.innerHTML.length+"</li>";
    valid = false;
  }else if(contenido.innerHTML.length==0){
    errorList += "<li>El artículo no puede estar vacío</li>";
    valid = false;
  }
  descripcion=document.getElementsByName("impactarte[descripcion]")[0];
  if (descripcion==undefined)
    descripcion = document.getElementsByName("articulo[descripcion]")[0];
    if(descripcion.value.length>600){
      errorList += "<li>La descripción es demasiado larga, el tamaño máximo son 600 caracteres, y el artículo actual tiene: "+descripcion.value.length+"</li>";
      valid=false;
    }
    else if (descripcion.value.length==0){
      errorList += "<li>La descripción no puede estar vacía</li>";
      valid = false;
    }
  if(!valid)
    showWarning(errorList);
  return valid;
}

function showWarning(errorList){
  $("#errorList").html(errorList);
  $("#alertModal").modal('show');
}
