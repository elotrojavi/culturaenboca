var async = require('async');

module.exports = function(){
  var autores_articulos = new Object();

  autores_articulos.GetByArticulosID = function(Id, connection, callback){
    connection.query("SELECT uID FROM autores_articulos WHERE aID = ?",[Id],function(err, rows){
      callback(err, rows);
    });
  }

  autores_articulos.Add = function(autor, articulo, connection, callback){
    connection.query("INSERT INTO autores_articulos (uID, aID) VALUES (?,?)", [autor,articulo],function(err, rows){
      callback(err, rows);
    });
  }

  autores_articulos.AddBulk = function(autorList, articulo, connection, callback){
    async.each(autorList,function(autor, asyncCallback){
      connection.query("INSERT INTO autores_articulos (uID, aID) VALUES (?,?)",[autor, articulo],function(err,rows){
        asyncCallback(err);
      });
    },function(err){
      callback(err, "ADDBULK");
    });
  }

  autores_articulos.Delete = function(autor, articulo, connection, callback){
    connection.query("DELETE FROM autores_articulos WHERE uID =? AND aID = ?", [autor, articulo], function(err,rows){
      callback(err);
    });
  }

  autores_articulos.DeleteBulk = function(autorList, articulo, connection, callback){
    async.each(autorList,function(autor, asyncCallback){
      connection.query("DELETE FROM autores_articulos WHERE uID =? AND aID = ?", [autor, articulo], function(err,rows){
        asyncCallback(err);
      });
    }, function(err){
      callback(err, "DELETEBULK");
    });
  }

  return autores_articulos;
}
