module.exports = function(pool){
    var categoria = new Object();
    categoria.GetAll = function(callback){
      pool.query("SELECT * FROM categoria ORDER BY categoria", function(err,rows){
        callback(err, rows);
      });
    };

    categoria.GetByID = function(ID, callback){
      pool.query("SELECT * FROM categoria WHERE ID = ? ORDER BY categoria",[ID],function(err, rows){
        callback(err, rows);
      });
    };

    categoria.GetByCategoria = function(categoria, callback){
      pool.query("SELECT ID, categoria FROM categoria WHERE categoria = ?",[categoria],function(err, rows){
        if(!rows.length){
          callback("Categoria no encontrada", null);
        }
        else
          callback(err, rows);
        });
    };

    categoria.GetByArticuloID = function(Id, callback){
      pool.query("SELECT c.ID, c.categoria FROM categoria c, articulos_categoria ac WHERE c.ID = ac.cID AND ac.aID = ?",[Id], function(err, rows){
        callback(err, rows);
      });
    }

    return categoria;
}
