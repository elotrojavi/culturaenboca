module.exports = function(pool){
    var imagenes = new Object();

    imagenes.GetAll = function(callback){
      pool.query("SELECT * FROM imagenes",function(err,rows){
        callback(err,rows);
      });
    };

    imagenes.GetById = function(Id, callback){
      pool.query("SELECT * FROM imagenes WHERE ID = ?",[Id], function(err, rows){
        callback(err, rows);
      });
    }

    imagenes.GetByUserId = function(UserId, callback){
      pool.query("SELECT i.ID, i.ruta, i.texto_alternativo FROM imagenes i, users u WHERE u.ID = i.uID AND u.ID = ?",[UserId], function(err, rows){
        callback(err,rows);
      });
    };

    imagenes.GetByIdAndUserId = function(Id, userId, callback){
          pool.query("SELECT ruta, texto_alternativo FROM imagenes WHERE uID = ? AND ID = ?",[userId,Id],function(err,rows){
            callback(err,rows);
          });
    };

    imagenes.Add = function (userId, image, callback){
      pool.query("INSERT INTO imagenes(ruta, texto_alternativo, uID) VALUES (?,?,?)",["/images/users/"+userId+"/"+image.filename,image.texto_alternativo,userId],function(err,rows){
        callback(err,rows);
      })
    };

    imagenes.DeleteById = function(Id, callback){
      pool.query("DELETE FROM imagenes WHERE ID = ?", [Id], function(err,rows){
        callback(err,rows);
      });
    }

    return imagenes;
}
