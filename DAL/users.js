module.exports = function(pool){
  var users = new Object;

  users.GetAll = function(callback){
    pool.query("SELECT *  FROM users ORDER BY name", function(err, rows){
      callback(err, rows);
    });
  };

  users.GetAllVisible = function(callback){
    pool.query("SELECT users.ID, name, surname, description, ruta, texto_alternativo, rol  FROM users, imagenes WHERE img=imagenes.ID AND visible = true ORDER BY name", function(err, rows){
      callback(err, rows);
    });
  };

  users.GetAllActive = function(callback){
    pool.query("SELECT name, surname, description, ruta, texto_alternativo, rol  FROM users, imagenes WHERE img=imagenes.ID AND activo = true ORDER BY name", function(err, rows){
      callback(err, rows);
    });
  };

  users.GetDistinct = function (userId, callback){
    pool.query("SELECT ID, name, surname, description, rol  FROM users WHERE users.ID != ? ORDER BY name",[userId],function(err, rows){
      callback(err, rows);
    });
  }

  users.GetByID = function(ID, callback){
    pool.query("SELECT users.ID, name, surname, email, rol, color, description,img, ruta, texto_alternativo FROM users, imagenes WHERE img=imagenes.ID AND users.ID = ? ORDER BY name",[ID],function(err, rows){
      callback(err, rows);
    });
  };

  users.GetByEmail = function(email, callback){
    pool.query("SELECT * FROM users WHERE email = ?",[email],function(err,rows){
      callback(err, rows);
    });
  }

  users.GetByFullName = function(name, surname, callback){
    pool.query("SELECT * FROM users WHERE name = ? AND surname = ?", [name,surname],function(err,rows){
      callback(err, rows);
    })
  }

  users.GetByArticuloID = function(articuloID, callback){
    pool.query("SELECT u.ID, u.name, u.surname, u.description, u.color, i.ruta, i.texto_alternativo FROM users u, autores_articulos aa, imagenes i WHERE u.ID = aa.uID AND aa.aID = ? AND i.ID = u.img",[articuloID] , function(err, rows){
      callback(err, rows);
    });
  };

  users.GetByProyectosID = function(proyectoID, callback){
    pool.query("SELECT u.ID, u.name, u.surname, u.description, u.color, i.ruta, i.texto_alternativo FROM users u, imagenes i, autores_proyecto ap WHERE u.ID = ap.uID AND ap.pID = ? AND i.ID = u.img",[proyectoID],function(err,rows){
      callback(err,rows);
    });
  };

  users.Add = function(user, callback){
    pool.query("INSERT INTO users ( email, password, name, surname, rol, activo, img) values (?,?,?,?,?,1,1)",[user.email, user.password, user.name, user.surname, user.rol],function(err,rows){
      callback(err, rows);
    });
  };

  users.Modify = function(user, callback){
    pool.query("UPDATE users SET activo = ?, rol=?, visible=? WHERE ID=?",[user.activo, user.rol,user.visible, user.ID],function(err,rows){
      callback(err, rows);
    });
  }

  users.UpdateWithoutPassword = function(user, callback){
    pool.query('UPDATE users SET name = ?, surname = ?, email = ?, description = ?, color=?, img=? WHERE ID = ?', [user.name, user.surname, user.email, user.description, user.color,user.img, user.id], function(err, rows){
      callback(err, rows);
    });
  }

  users.UpdateWithPassword = function(user, callback){
    pool.query('UPDATE users SET name = ?, surname = ?, email = ?, description = ?, password = ?, color= ?, img = ? WHERE ID = ?', [user.name, user.surname, user.email, user.description,user.password, user.color ,user.img,user.id], function(err, rows){
      callback(err, rows);
    });
  }

  users.DeleteByID = function(ID, callback){
    pool.query("DELETE FROM users WHERE ID = ?",[ID],function(err, rows){
      callback(err, rows);
    });
  };

  return users
}
