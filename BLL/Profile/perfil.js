var bcrypt = require('bcrypt-nodejs');

module.exports = function(app, dal, passport){

  var authHelper = require('../authenticationHelper.js')(dal);
  app.get('/my-account/perfil', authHelper.isLoggedIn, function(req,res){
    dal.users.GetByID(req.session.passport.user,function(err,rows){
      if(err)
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
      res.render('profile/perfil', {user:rows[0], errors: req.flash('error'), successes: req.flash('correct')});
    });
  });

  app.post('/my-account/perfil', authHelper.isLoggedIn,function(req, res){
    var user = setUser(req);
    if (user.password === ''){
      dal.users.UpdateWithoutPassword(user, function(err, rows){
        if (err)
          req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
      else
        req.flash('correct', 'Se han guardado los cambios correctamente');
        res.redirect('/my-account/perfil');
      });
    } else {
      user.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
      dal.users.UpdateWithPassword(user, function(err, rows){
        if (err)
          req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        else
          req.flash('correct', 'Se han guardado los cambios correctamente');
        res.redirect('/my-account/perfil');
      });
    }
  });
}

function setUser(req){
  var user = new Object();
  user.name = req.body.name;
  user.surname= req.body.surname;
  user.email = req.body.email;
  user.description = req.body.description;
  user.password = req.body.password;
  user.color = req.body.color;
  user.img = req.body.img;
  user.id = req.session.passport.user;
  return user;
}
