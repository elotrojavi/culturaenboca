module.exports = function(app, dal, passport){

  var authHelper = require('../authenticationHelper.js')(dal);

  app.get("/my-account", authHelper.isLoggedIn, function(req, res){
    res.redirect("/my-account/articulos");
  });

  // Rutas de articulos
  require("./articulos.js")(app, dal, passport);
  // Rutas de Eventos
  require("./eventos.js")(app,dal,passport);
  //Rutas de Impactarte
  require("./impactarte.js")(app,dal,passport);
  //Rutas de Proyectarte
  require("./proyectarte.js")(app,dal,passport);
  //Rutas de imagenes
  require("./imagenes.js")(app,dal,passport);
  // Rutas de gestion de usuarios
  require("./users.js")(app,dal,passport);
  // Rutas de gestion de perfil
  require("./perfil.js")(app,dal,passport);
}
