var express         = require("express")
    app             = express (),
    session         = require('express-session'),
    cookieParser    = require('cookie-parser'),
    bodyParser      = require("body-parser"),
    methodOverride  = require("method-override"),
    path            = require("path"),
    fs              = require('fs'),
    flash           = require('connect-flash'),
    helmet          = require('helmet'),
    express_ssl     = require('express-enforces-ssl'),
    https           = require('https');

var passport = require('passport')

var options = {
  key: fs.readFileSync(path.join(__dirname+'/config/ssl/server.key'), 'utf8'),
  cert: fs.readFileSync(path.join(__dirname+'/config/ssl/server.crt'), 'utf8')
}

var dal = require("./DAL/dataAccesMain.js");
require('./config/passport.js')(passport, dal);

app.set("view engine", "ejs");
app.use(helmet());
app.use(express_ssl());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname+"/public"));
app.use(methodOverride("_method"));

app.use(session({
	secret: 'culturababeadaonobabeadaquiensabe',
	resave: true,
	saveUninitialized: false
 } )); // session secret
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());


require('./config/routes.js')(app, dal, passport);

https.createServer(options, app).listen(443, function(){
  console.log('Servidor de Culturaenboca con ssl en marcha');
});
app.listen(80, function(){
  console.log("The Culturaenboca Server Has Started");
})
