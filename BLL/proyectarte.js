var async = require('async');

module.exports = function(app, dal, passport){
  app.get('/proyectartes', function(req,res){
    dal.proyectos.GetPublished(1, function(err, rows){
      if(err){
        console.log(err);
        res.redirect("/");
      } else {
        res.render("proyectos/proyectartes", {impactartes:rows}) ;
      }
    });
  });

  app.get('/proyectartes/:pID', function(req, res){
    titulo = decodeURIComponent(req.params.pID).split("_").join(" ");
    async.waterfall([
      function(callback){
        dal.proyectos.GetByTitle(titulo, callback);
      },
      function(impactarte, callback){
        dal.users.GetByProyectosID(impactarte[0].ID, function(err, rows){
          if(!err){
            impactarte[0].autores=rows;
          }
          callback(err, impactarte)
        });
      }
    ], function(err,results){
      if(err){
        console.log(err);
        res.redirect("/proyectartes");
      }
      var color= "#000000";
      if(results[0].autores.length===1)
        color = results[0].autores[0].color;
      res.render("proyectos/proyectarte",{impactarte:results[0  ], color:color});
    });
  });
}
