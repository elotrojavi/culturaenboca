module.exports = function (app, dal, passport) {
  // Main Page routes
  require("../BLL/mainpage.js")(app, dal, passport);
  // Authentication routes
  require("../BLL/authentication.js")(app, dal, passport);
  //User routes
  require("../BLL/Profile/profileRoutes.js")(app, dal, passport);
  require("../BLL/impactarte.js")(app, dal, passport);
  require("../BLL/proyectarte.js")(app, dal, passport);
  require("../BLL/informarte.js")(app, dal, passport);
}
