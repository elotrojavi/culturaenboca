var async = require('async');

module.exports = function(){
  var autores_proyecto = new Object();

  autores_proyecto.GetByProyectosID = function(Id, connection, callback){
    connection.query("SELECT uID FROM autores_proyecto WHERE pID = ?",[Id],function(err, rows){
      callback(err, rows);
    });
  }

  autores_proyecto.Add = function(autor, proyecto, connection, callback){
    connection.query("INSERT INTO autores_proyecto (uID, pID) VALUES (?,?)", [autor,proyecto],function(err, rows){
      callback(err, rows);
    });
  }

  autores_proyecto.AddBulk = function(autorList, proyecto, connection, callback){
    async.each(autorList,function(autor, asyncCallback){
      connection.query("INSERT INTO autores_proyecto (uID, pID) VALUES (?,?)",[autor, proyecto],function(err,rows){
        asyncCallback(err);
      });
    },function(err){
      callback(err, "ADDBULK");
    });
  }

  autores_proyecto.Delete = function(autor, proyecto, connection, callback){
    connection.query("DELETE FROM autores_proyecto WHERE uID =? AND pID = ?", [autor, proyecto], function(err,rows){
      callback(err);
    });
  }

  autores_proyecto.DeleteBulk = function(autorList, proyecto, connection, callback){
    async.each(autorList,function(autor, asyncCallback){
      connection.query("DELETE FROM autores_proyecto WHERE uID =? AND pID = ?", [autor, proyecto], function(err,rows){
        asyncCallback(err);
      });
    }, function(err){
      callback(err, "DELETEBULK");
    });
  }

  return autores_proyecto;
}
