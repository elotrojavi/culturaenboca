var async             = require('async'),
autores_proyecto  = require("./autores_proyecto.js")();

module.exports = function(pool){
  var proyectos = new Object();

  proyectos.GetAll = function(callback){
    pool.query("SELECT p.titulo,p.descripcion, p.ID, i.ruta, i.texto_alternativo FROM proyectos p, imagenes i WHERE p.portada = i.ID ORDER BY fecha_publicacion DESC", function(err, rows){
      callback(err, rows);
    });
  }

  //Type indica si se quiere recoger los proyectartes (type=1) o los impactartes (type=0)
  proyectos.GetPublished = function(type, callback){
    pool.query("SELECT p.titulo,p.descripcion, p.ID, i.ruta, i.texto_alternativo FROM proyectos p, imagenes i WHERE p.portada = i.ID AND p.proyectarte=? AND p.publicado=1 ORDER BY fecha_publicacion DESC",[type], function(err, rows){
      callback(err, rows);
    });
  }

  proyectos.GetById = function(Id, callback){
    pool.query("SELECT p.ID, p.titulo, p.texto, i.ruta FROM proyectos p, imagenes i WHERE p.publicado=1 AND p.proyectarte=0 AND p.ID = ? AND p.portada = i.ID",[Id], function(err,rows){
      callback(err,rows);
    });
  }

  proyectos.GetByTitle = function(title, callback){
    pool.query("SELECT p.ID, p.titulo, p.texto, i.ruta FROM proyectos p, imagenes i WHERE p.publicado=1 AND p.titulo=? AND p.portada = i.ID",[title], function(err,rows){
      callback(err,rows);
    });
  }

  proyectos.GetByUserId = function(userId, type, callback){
    pool.query("SELECT p.ID, p.titulo, p.descripcion, i.ruta, i.texto_alternativo, p.publicado FROM proyectos p, imagenes i, autores_proyecto ap WHERE p.proyectarte = ? AND ap.pID = p.ID AND i.ID=p.portada AND ap.uID = ?  ORDER BY p.fecha_publicacion DESC", [type, userId], function(err, rows){
      callback(err, rows);
    });
  }

  proyectos.GetPublishedByUserId = function(userId, type, callback){
    pool.query("SELECT p.ID, p.titulo, p.descripcion, i.ruta, i.texto_alternativo, p.publicado FROM proyectos p, imagenes i, autores_proyecto ap WHERE p.proyectarte = ? AND ap.pID = p.ID AND i.ID=p.portada AND ap.uID = ? AND p.publicado = 1  ORDER BY p.fecha_publicacion DESC", [type, userId], function(err, rows){
      callback(err, rows);
    });
  }

  proyectos.GetByIdAndUserId = function(Id, userId, callback){
    pool.query("SELECT p.ID, p.titulo, p.texto, p.descripcion, p.portada, i.ruta FROM proyectos p, imagenes i, autores_proyecto ap WHERE p.ID = ? AND p.ID = ap.pID AND ap.uID = ? AND p.portada = i.ID", [Id, userId], function(err, rows) {
      callback(err,rows);
    });
  };

  proyectos.Insert = function(proyecto, connection, callback){
    connection.query("INSERT INTO proyectos (titulo, texto, proyectarte, fecha_publicacion, publicado, portada, descripcion) VALUES (?,?,?,?,?,?,?)",[proyecto.titulo, proyecto.articulo,proyecto.tipo ,proyecto.fecha, proyecto.publish, proyecto.portada, proyecto.descripcion], function(err, rows){
      callback(err, rows);
    });
  }

  proyectos.Add = function(proyecto, callback){
    pool.getConnection(function(err,connection){
      async.series([
        function(asyncCallback){
          connection.beginTransaction(function(err){
            asyncCallback(err, "TRANSACTION");
          });
        },
        function(asyncCallback){
          Insertions(proyecto, connection, asyncCallback)
        },
        function(callback){
          connection.commit(function(err){
            callback(err, "COMMIT");
          });
        }
      ], function(err, results){
        if(err)
        connection.rollback();
        connection.release();
        callback(err, results);
      });
    });
  }

  proyectos.Publish = function(Id, publish, callback){
    pool.query("UPDATE proyectos SET publicado = ?, fecha_publicacion = ? WHERE ID = ? ",[publish,new Date(), Id], function(err, rows){
      callback(err, rows);
    });
  }

  proyectos.Modify = function (proyecto, connection, callback){
    connection.query("UPDATE proyectos SET titulo = ?, texto = ?, portada = ?, descripcion= ? WHERE ID = ?",[proyecto.titulo, proyecto.articulo,proyecto.portada,proyecto.descripcion,proyecto.id], function(err,rows){
      callback(err, rows)
    });
  }

  proyectos.Update = function (proyecto, callback){
    pool.getConnection(function(err,connection){
      async.series([
        function(asyncCallback){
          connection.beginTransaction(function(err){
            asyncCallback(err, "TRANSACTION");
          });
        },
        function(asyncCallback){
          UpdateProcess(proyecto, connection, asyncCallback)
        },
        function(callback){
          connection.commit(function(err){
            callback(err, "COMMIT");
          });
        }
      ], function(err, results){
        if(err)
        connection.rollback();
        connection.release();
        callback(err, results);
      });
    });
  }

  proyectos.DeleteById = function(Id, callback){
    pool.query("DELETE FROM proyectos WHERE ID = ?",[Id],function(err, rows){
      callback(err, rows);
    });
  }
  //Métodos auxiliares
  function Insertions(proyecto, connection, callback){
    async.waterfall([
      function(waterfallCallback){
        proyectos.Insert(proyecto, connection, waterfallCallback);
      },
      function(article, waterfallCallback){
        autores_proyecto.AddBulk(proyecto.users, article.insertId, connection, waterfallCallback);
      }
    ],function(err, result){
      callback(err, "INSERTIONS");
    });
  }

  function UpdateProcess(proyecto, connection, callback){
    async.parallel([
      function(asyncCallback){
        proyectos.Modify(proyecto, connection, asyncCallback);
      },
      function(asyncCallback){
        UpdateAuthorsProcess(proyecto.id, proyecto.users, connection, asyncCallback);
      },
    ], function(err, results){
      callback(err, results);
    });
  }

  function UpdateAuthorsProcess(Id, users, connection, callback){
    async.waterfall([
      function(asyncCallback){
        //Se obtienen los autoes del proyecto almacenados en la BBDD
        autores_proyecto.GetByProyectosID(Id, connection, asyncCallback);
      },
      function(autoresBBDD, asyncCallback){
        //Se pasan los autores de la BBDD a un array solo con los ID para facilitar
        var autoresOld = new Array();
        autoresBBDD.forEach(function(autor){autoresOld.push(autor.uID)});
        var autoresAnadir = GetAddedAuthors(autoresOld, users);
        var autoresBorrar = GetDeletedAuthors(autoresOld, users);
        asyncCallback(null, autoresAnadir, autoresBorrar);
      },
      function(autoresAnadir, autoresBorrar, asyncCallback){
        UpdateAuthors(Id, autoresAnadir, autoresBorrar, connection, asyncCallback)
      }
    ],function(err ,results){
      callback(err,results);
    })
  }

  function GetAddedAuthors(autoresOld, autoresNew){
    var addedAuthors = new Array();
    autoresNew.forEach(function(autor){
      if(!autoresOld.includes(autor))
      addedAuthors.push(autor);
    });
    return addedAuthors;
  }

  function GetDeletedAuthors(autoresOld, autoresNew){
    var deletedAuthors = new Array();
    autoresOld.forEach(function(autor){
      if(!autoresNew.includes(autor))
      deletedAuthors.push(autor);
    });

    return deletedAuthors;
  }

  function UpdateAuthors(Id, addedAuthors, deletedAuthors, connection, callback){
    async.parallel([
      function(asyncCallback){
        autores_proyecto.AddBulk(addedAuthors, Id, connection, asyncCallback);
      },
      function(asyncCallback){
        autores_proyecto.DeleteBulk(deletedAuthors, Id, connection, asyncCallback);
      }
    ],function(err, results){
      callback(err, results);
    });
  }

  return proyectos;
}