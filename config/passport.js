var LocalStrategy = require('passport-local').Strategy;

var bcrypt = require('bcrypt-nodejs');

  module.exports = function (passport , dal){

    passport.serializeUser(function(user,done){
      done(null, user.ID);
    });

    passport.deserializeUser(function(id, done){
      dal.users.GetByID(id, function(err, rows){
        done(err, rows[0]);
      });
    });

    passport.use(
        'local-signup',
        new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'email',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, email, password, done) {
            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            dal.users.GetByEmail(email, function(err, rows) {
                if (err){
                  console.log(err);
                  return done(err);
                }
                if (rows.length) {
                    return done(null, false);
                } else {
                    // if there is no user with that username
                    // create the user
                    var newUserMysql = {
                        email: email,
                        password: bcrypt.hashSync(email, bcrypt.genSaltSync(10)),
                        name : req.body.name,
                        surname : req.body.surname,
                        rol : req.body.rol
                    };
                    dal.users.Add(newUserMysql,function(err, rows) {
                      if(err){
                        console.log(err);
                      }
                        newUserMysql.ID = rows.insertId;

                        return done(null, newUserMysql);
                    });
                }
            });
        })
    );

    passport.use(
        'local-login',
        new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'email',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, email, password, done) { // callback with email and password from our form
            dal.users.GetByEmail(email, function(err, rows){
                if (err)
                    return done(err);
                if (!rows.length) {
                    return done(null, false, req.flash('loginMessage', 'No existe ningún usuario con ese e-mail')); // req.flash is the way to set flashdata using connect-flash
                }
                if (rows[0].activo===0)
                  return done(null, false);
                // if the user is found but the password is wrong
                if (!bcrypt.compareSync(password, rows[0].password)){
                    return done(null, false, req.flash('loginMessage', 'La contraseña introducida es incorrecta'));} // create the loginMessage and save it to session as flashdata
                // all is well, return successful user
                return done(null, rows[0]);
            });
        })
    );
  }
