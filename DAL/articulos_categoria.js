var async = require('async');

module.exports = function(){
  var articulos_categoria = new Object();

  articulos_categoria.GetByArticulosID = function(Id, connection, callback){
    connection.query("SELECT cID FROM articulos_categoria WHERE aID = ?",[Id],function(err, rows){
      callback(err, rows);
    });
  }

  articulos_categoria.Add = function(articulo, categoria, connection, callback){
    connection.query("INSERT INTO articulos_categoria (aID, cID) VALUES (?,?)", [articulo,categoria],function(err, rows){
      callback(err, rows);
    });
  }

  articulos_categoria.AddBulk = function(categoriaList, articulo, connection, callback){
    async.each(categoriaList,function(categoria, asyncCallback){
      connection.query("INSERT INTO articulos_categoria (aID, cID) VALUES (?,?)",[articulo, categoria],function(err,rows){
        asyncCallback(err);
      });
    },function(err){
      callback(err, "ADDBULK");
    });
  }

  articulos_categoria.Delete = function(articulo, categoria, connection, callback){
    connection.query("DELETE FROM articulos_categoria WHERE aID =? AND cID = ?", [articulo, categoria], function(err,rows){
      callback(err);
    });
  }

  articulos_categoria.DeleteBulk = function(categoriaList, articulo, connection, callback){
    async.each(categoriaList,function(categoria, asyncCallback){
      connection.query("DELETE FROM articulos_categoria WHERE aID =? AND cID = ?", [articulo, categoria], function(err,rows){
        asyncCallback(err);
      });
    }, function(err){
      callback(err, "DELETEBULK");
    });
  }

  return articulos_categoria;
}
