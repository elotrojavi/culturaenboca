var async = require('async'),
multer      = require('multer'),
fs          = require('fs-extra');

var storage = multer.diskStorage({
  destination: function(req, file, cb){
    cb(null, './public/images/users/'+req.session.passport.user);
  },
  filename: function(req,file,cb){
    cb(null, file.originalname);
  }
});

var upload = multer({storage:storage}).single('imagen');

module.exports = function(app, dal, passport){

  var authHelper = require('../authenticationHelper.js')(dal);

  app.get('/my-account/imagenes', authHelper.isLoggedIn, function(req,res){
    async.parallel({
      rol:function(callback){
        dal.rol.GetByUserId(req.session.passport.user, callback);
      },
      imagenes:function(callback){
        dal.imagenes.GetByUserId(req.session.passport.user, callback);
      }
    }, function(err, results){
      if(err){
        req.flash("error", "Ha habido un error, por favor intentelo de nuevo");
        res.redirect("/my-account");
      }
      res.render("profile/imagenes/imagenes", {imagenes:results.imagenes, rol:results.rol[0].rol, errors:req.flash("error"), successes:req.flash("correct")});
    });
  });

  app.post('/my-account/imagenes', authHelper.isLoggedIn, function(req,res){
    async.series([
      function(callback){
        upload(req,res, function(err){
          callback(err,'ONE');
        });
      },
      function(callback){
        var imagen = new Object();
        imagen.filename = req.file.filename;
        imagen.texto_alternativo = req.body.texto_alternativo;
        dal.imagenes.Add(req.session.passport.user, imagen, callback);
      }
    ], function(err, results){
      if(err){
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        console.log(err);
      }
      else
        req.flash('correct', 'La imagen se ha subido correctamente');
      res.redirect("/my-account/imagenes");
    });
  });

  app.delete('/my-account/imagenes/:id', authHelper.isLoggedIn, function(req,res){
    async.series({
      comprobación: function(callback){
        dal.imagenes.GetById(req.params.id,function(err, rows){
          if(err)
            callback(err, rows);
          else {
            if(rows.length===0)
              callback("Esta imagen no te pertenece",null);
            callback(err, rows);
          }
        });
      },
      imagen: function(callback){
        dal.imagenes.GetById(req.params.id, callback);
      },
      borrado : function(callback){
        dal.imagenes.DeleteById(req.params.id, callback);
      }
    }, function(err, results){
      if(err){
        console.log(err);
        req.flash('error', 'No puedes borrar esta imagen porque está en uso');
      } else {
        req.flash('correct', 'La imagen se ha eliminado correctamente');
        fs.remove("./public/"+results.imagen[0].ruta);
      }
      res.redirect("/my-account/imagenes");
    });
  });

  app.get('/api/imagenes', authHelper.isLoggedIn, function(req,res){
    dal.imagenes.GetByUserId(req.session.passport.user, function(err,rows){
      if(err){
        console.log(err);
      }
      else{
        res.json(rows);
      }
    });
  });

  app.get('/api/imagenes/:id', authHelper.isLoggedIn, function(req,res){
    dal.imagenes.GetByIdAndUserId(req.params.id, req.session.passport.user,function(err,rows){
      if (err || !rows.length)
        console.log(err);
      else{
        res.json(rows[0]);
      }
    });
  });
}
