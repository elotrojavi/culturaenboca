var async = require('async');

module.exports = function(app, dal, passport){
  app.get("/", function(req, res){
    res.render('landing');
  });

  app.get("/queEsCulturaenboca", function(req, res){
    res.render('queEsCulturaenboca')
  });

  app.get("/quienesSomos", function(req, res){
    dal.users.GetAllVisible(function(err, rows){
      if (err){
        req.flash ('databaseError', 'Ha habido un error al cargar los datos, prueba recarga la página')
        res.redirect("/")
      }
      res.render('quienesSomos', {personas: rows, errors:req.flash('databaseError'), successes:req.flash('correct')});
    });
  });

  app.get("/terminosycondiciones", function(req, res){
    res.render('avisos/terminosycondiciones');
  });

  app.get("/avisolegal", function(req, res){
    res.render('avisos/avisolegal');
  });

  app.get("/contacto", function(req, res){
    res.render('avisos/contacto');
  });

  app.get("/autores/:user", function(req, res){
    async.waterfall([
      function(callback){
        var user = decodeURIComponent(req.params.user).split("_").join(" ").split("-");
        dal.users.GetByFullName(user[0], user[1], callback);
      },
      function(user, callback){
        getAllWorksByUserId(user[0].ID, callback);
      }
    ],
    function(err, results){
      if (err){
        console.log(err);
      }
      res.render('autor.ejs', {articulos:results.articulos, autor:results.autor[0], impactartes:results.impactartes, proyectartes:results.proyectartes});
    });
  });


  function getAllWorksByUserId(userId, callback) {
    async.parallel({
      autor: function(asynCallback){
        dal.users.GetByID(userId, asynCallback);
      },
      articulos: function(asynCallback){
        dal.articulos.GetPublishedByUserId(userId, asynCallback);
      },
      proyectartes: function(asynCallback){
        dal.proyectos.GetPublishedByUserId(userId, 1, asynCallback);
      },
      impactartes: function(asynCallback){
        dal.proyectos.GetPublishedByUserId(userId, 0, asynCallback);
      }
    }, function(err, results){
      callback(err, results);
    });
  }
}
