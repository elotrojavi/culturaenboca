var async = require('async'),
fs        = require('fs-extra'),
bcrypt      = require('bcrypt-nodejs');

module.exports = function(app, dal, passport){

  var authHelper = require('../authenticationHelper.js')(dal);

  app.get("/my-account/users", authHelper.isAdmin, function(req,res){
    async.parallel({
      roles:function(callback){
        dal.rol.GetAll(callback);
      },
      users:function(callback){
        dal.users.GetAll(callback);
      }
    },function(err, results){
      if (err)
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
      res.render('profile/users/users',{roles: results.roles,users:results.users, errors:req.flash('error'), successes:req.flash('correct')});
    });
  });

  app.get("/my-account/users/new", authHelper.isAdmin, function(req,res){
    dal.rol.GetAll(function(err,rows){
      if(err){
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        res.redirect("/my-account/users");
      }
      else
        res.render("profile/users/new",{roles:rows, messages:req.flash('error')});
    });
  });

  app.post('/my-account/users', authHelper.isAdmin, function(req,res){
    req.body.user.password = bcrypt.hashSync(req.body.user.email, bcrypt.genSaltSync(10));
    dal.users.Add(req.body.user,function(err, rows){
      if (err){
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        console.log(err);
      }
      else{
        req.flash('correct', 'El usuario se ha creado correctamente');
        if(!fs.existsSync("./public/images/users/"+rows.insertId))
          fs.mkdirSync("./public/images/users/"+rows.insertId);
      }
      res.redirect("/my-account/users");
    });
  });

  app.put("/my-account/users/:id", authHelper.isAdmin, function(req,res){
    var user = new Object();
    user.activo = req.body.activo ? 1 : 0;
    user.visible = req.body.visible ? 1 : 0;
    user.ID = req.params.id;
    user.rol = req.body.rol;
    dal.users.Modify(user,function(err,rows){
      if(err){
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        console.log(err);
      }
      else
        req.flash('correct', 'El usuario se ha modificado correctamente');
      res.redirect("/my-account/users");
    });
  });

  app.delete("/my-account/users/:id", authHelper.isAdmin, function(req,res){
    dal.users.DeleteByID(req.params.id,function(err, rows){
      if(err)
        req.flash('error', 'No se puede eliminar a este usuario');
      else{
        req.flash('correct', 'El usuario se ha eliminado correctamente');
        fs.remove('./public/images/users/'+req.params.id);
      }

      res.redirect("/my-account/users");
    });
  });
}
