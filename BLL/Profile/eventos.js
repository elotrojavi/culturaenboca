var async = require('async');


module.exports = function(app, dal, passport){

  var authHelper = require('../authenticationHelper.js')(dal);

  app.get("/my-account/eventos", authHelper.isMember, function(req,res){
    async.parallel({
      eventos:function(callback){
        dal.eventos.GetByUserId(req.session.passport.user, callback);
      },
      rol:function(callback){
        dal.rol.GetByUserId(req.session.passport.user, callback);
      }
    },function(err, results){
      if(err){
        req.flash('error','Ha habido un error, por favor intentelo de nuevo');
        console.log(err);
        res.redirect("/my-account")
      } else {
        res.render("profile/eventos/index", {eventos:results.eventos, rol:results.rol[0].rol, errors:req.flash('error'), successes:req.flash('correct')});
      }
    });
  });

  app.get("/my-account/eventos/new", authHelper.isMember, function(req,res){
    res.render("profile/eventos/new");
  });

  app.post("/my-account/eventos", authHelper.isMember, function(req,res){
    evento = req.body.evento;
    if(evento.link=="")
      evento.link = null;
    evento.autor = req.session.passport.user;
    dal.eventos.Add(evento,function(err,rows){
      if (err){
        req.flash('error', 'Ha habido un problema y no se ha guardado el evento');
        console.log(err);
      }
      else
        req.flash('correct', 'El evento se ha creado correctamente');
      res.redirect('/my-account/eventos');
    });
  });

  app.delete('/my-account/eventos/:id', authHelper.isMember, function(req, res){
    async.series([
      function(callback){
        dal.eventos.GetByIdAndUserId(req.params.id, req.session.passport.user, function(err, rows){
          if(err)
            callback(err, null);
          else
            if (rows.length===0)
              callback("Este evento no te pertenece", null);
            else
              callback(null, "ONE");
        });
      },
      function(callback){
        dal.eventos.DeleteById(req.params.id,callback);
      }
    ],function(err, results){
      if(err)
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
      else
        req.flash('correct', 'El evento se ha eliminado correctamente');
      res.redirect("/my-account/eventos");
    });

  });

  app.get('/my-account/eventos/edit/:id', authHelper.isMember, function(req, res){
    async.series({
      comprobacion : function(callback){
        dal.eventos.GetByIdAndUserId(req.params.id,req.session.passport.user,function(err, rows){
          if(err)
            callback(err, null);
          else
            if (rows.length===0)
              callback("Este evento no te pertenece", null);
            else
              callback(null, "ONE");
        });
      },
      evento : function(callback){
        dal.eventos.GetById(req.params.id, callback);
      }
    }, function(err, results){
      if(err){
        req.flash('error','Ha habido un error, por favor intentelo de nuevo');
        res.redirect('/my-account/eventos');
      } else {
        res.render("profile/eventos/edit", {evento:results.evento[0]});
      }
    });
  });

  app.put('/my-account/eventos/:id', authHelper.isMember, function(req, res){
    async.series([
      function(callback){
        dal.eventos.GetByIdAndUserId(req.params.id, req.session.passport.user,function(err, rows){
          if(err)
            callback(err, null);
          else
            if (rows.length===0)
              callback("Este evento no te pertenece", null);
            else
              callback(null, "ONE");
        });
      },
      function(callback){
        evento = req.body.evento;
        if(req.body.publish)
          dal.eventos.Publish(req.params.id, req.body.publish, callback);
        else
          dal.eventos.UpdateById(req.params.id, evento, callback);
      }
    ], function(err, result){
      if(err){
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        console.log(err);
      }
      else
        req.flash('correct', 'El evento se ha modificado correctamente');
      res.redirect("/my-account/eventos");
    });

  });
}
