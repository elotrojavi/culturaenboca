var async = require('async');

module.exports = function(app, dal, passport){

  app.get("/informarte", function(req, res){
    async.parallel({
      categorias:dal.categoria.GetAll,
      ultArticulos:dal.articulos.GetRecientes
    }, function(err, results){
      if(err)
        req.flash ('databaseError', 'Ha habido un error al cargar los datos, prueba recarga la página')
      res.render("informarte", {categorias:results.categorias, articulos:results.ultArticulos, errors:req.flash('databaseError'), successes:req.flash('correct')});
    });
  });

  app.get("/informarte/articulos", function (req, res) {
    dal.articulos.GetAll(function (err, rows) {
      if (err)
        req.flash('databaseError', 'Ha habido un error al cargar los datos, prueba recarga la página')
      res.render("articulos/articulos", { articulos: rows, errors: req.flash('databaseError'), successes: req.flash('correct') });
    });
  });

  app.get('/eventos', function(req,res){
    dal.eventos.GetAll(function(err, rows){
      if(err){
        console.log(err);
        res.end();
      } else
        res.render("eventos", {eventos: rows});
    });
  });

  app.get("/categorias", function (req, res) {
    dal.categoria.GetAll(function(err, rows){
      if (err)
        req.flash('databaseError', 'Ha habido un error al cargar los datos, prueba recarga la página')
      res.render("categorias", { categorias: rows, errors: req.flash('databaseError'), successes: req.flash('correct') });
    });
  });

  app.get('/categorias/:cID', function(req,res){
    async.waterfall([
      function(callback){
        dal.categoria.GetByCategoria(req.params.cID, callback);
      },
      function(categoria, callback){
        dal.articulos.GetByCategoria(categoria[0].ID,function(err,rows){
          results = new Object();
          results.categoria = categoria;
          results.articulos = rows;
          callback(err, results);
        });
      }
    ],function(err,results){
      if(err){
        console.log(err);
        res.redirect("/");
      }
      else{
        res.render('articulos/categoria',{articulos:results.articulos, categoria:results.categoria[0]});
      }
    });
  });

  app.get('/informarte/:aID', function(req,res){
    async.waterfall([
      function(callback){
        titulo = decodeURIComponent(req.params.aID).split("_").join(" ");
          dal.articulos.GetByTitulo( titulo, function(err, rows){
            if(!rows.length){
              callback("Articulo no encontrado", null);
            } else{
              callback(err,rows[0]);
            }
          });
      },
      function(articulo, callback){
        dal.users.GetByArticuloID(articulo.ID, function(err, rows){
          articulo.autores = rows;
          callback(err, articulo);
        })
      }
    ], function (err, results){
      if(err){
        console.log(err);
        res.redirect("/categorias");
      } else{
        var color = "#00000";
        if(results.autores.length===1)
          color = results.autores[0].color;
        res.render("articulos/articulo",{articulo:results, color:color});
      }
    });
  });

  /*app.get('/:cID/:aID', function(req,res){
    async.waterfall([
      function(callback){
        dal.categoria.GetByCategoria(req.params.cID,function(err,rows){
          if(err||!rows.length){
            callback("Categoria no encontrada", null);
          } else {
            callback(err,rows[0]);
          }
        });
      },
      function(categoria, callback){
        titulo = decodeURIComponent(req.params.aID).split("_").join(" ");
        dal.articulos.GetByTituloAndCategoria(titulo, categoria.ID, function(err, rows){
            if(!rows.length){
              callback("Articulo no encontrado", null);
            } else{
              callback(err, categoria, rows[0]);
            }
          });
      },
      function(categoria, articulo, callback){
        dal.users.GetByArticuloID(articulo.ID, function(err, rows){
          articulo.autores = rows;
          articulo.categoria = categoria;
          callback(err, articulo);
        });
      }
    ], function (err, results){
      if(err){
        console.log(err);
        res.redirect("/");
      } else{
        var color = "#00000";
        if(results.autores.length===1)
          color = results.autores[0].color;
      res.render("articulos/articulo",{articulo:results, color:color, categoria:results.categoria});
      }
    });
  });*/

  app.get('/*',function(req,res){
    res.redirect("/");
  });
}
