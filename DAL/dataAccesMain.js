var mysql   = require('mysql'),
dbconfig    = require('../config/database.js');
var pool  = mysql.createPool(dbconfig.connection);
var dataAccess = new Object();

dataAccess.articulos = require('./articulos.js')(pool);
dataAccess.categoria = require('./categoria.js')(pool);
dataAccess.eventos = require('./eventos.js')(pool);
dataAccess.imagenes = require('./imagenes.js')(pool);
dataAccess.proyectos = require('./proyectos.js')(pool);
dataAccess.rol = require('./rol.js')(pool);
dataAccess.users = require('./users.js')(pool);


module.exports = dataAccess;
