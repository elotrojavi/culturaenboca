var async = require('async');

module.exports = function(app, dal, passport){

  var authHelper = require('../authenticationHelper.js')(dal);

  app.get('/my-account/articulos', authHelper.isLoggedIn, function(req, res){
    async.parallel({
      articulos:function(callback){
        dal.articulos.GetByUserId(req.session.passport.user, callback);
      },
      rol:function(callback){
        dal.rol.GetByUserId(req.session.passport.user, callback);
      }
    }, function(err,results){
      if(err){
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        console.log(err);
        res.redirect("/my-account/perfil");
      }
      else{
        res.render("profile/articulos",{articulos:results.articulos, rol:results.rol[0].rol, errors:req.flash('error'), successes:req.flash('correct')});
      }
    });
  });

  app.get('/my-account/articulos/new', authHelper.isLoggedIn, function(req, res){
    async.parallel({
      categorias:dal.categoria.GetAll,
      users:function(callback){
        dal.users.GetDistinct(req.session.passport.user, callback);
      }
    }, function(err, results){
      if(err){
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        res.redirect("/my-account/articulos");
      }
      else{
        res.render("profile/articulos/new", {categorias:results.categorias, users:results.users});
      }
    });
  });

  app.post("/my-account/articulos", authHelper.isLoggedIn, function(req, res){
    var articulo = setArticulo(req.body.articulo, req.session.passport.user);
    dal.articulos.Add(articulo, function(err, results){
        if (err){
          console.log(err);
          req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        }
        else
          req.flash('correct', 'Se ha creado el artículo correctamente');
        res.redirect("/my-account/articulos");
      });
  });

  app.get("/my-account/articulos/:id", authHelper.isLoggedIn, function(req, res){
    async.parallel({
      articulo: function(callback){
        dal.articulos.GetByIdAndUserId(req.params.id, req.session.passport.user, callback);
      },
      autores: function(callback){
        dal.users.GetByArticuloID(req.params.id, callback);
      }
    },function(err, results){
      if(err || !results.articulo || !results.autores || results.articulo.length==0){
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        res.redirect("/my-account/articulos");
      }
      else{
        results.articulo[0].autores = results.autores;
        var color = "#000000";
        if(results.autores.length===1)
          color = results.autores[0].color;
        res.render("profile/articulos/show",{articulo:results.articulo[0], color:color});
      }
    });
  });

  app.delete('/my-account/articulos/:id', authHelper.isLoggedIn, function(req,res){
    async.series([
      function(callback){
        dal.articulos.GetByIdAndUserId(req.params.id,req.session.passport.user, function(err, rows){
          if(err)
            callback(err, null);
          else
            if (rows.length===0)
              callback("Este articulo no te pertenece", null);
            else
              callback(null, "ONE");
        });
      },
      function(callback){
        dal.articulos.DeleteById(req.params.id, callback);
      }
    ],function(err, results){
      if(err)
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
      else
        req.flash('correct', 'Se ha eliminado el artículo correctamente');
      res.redirect("/my-account/articulos");
    });
  });

  app.get("/my-account/articulos/edit/:id", authHelper.isLoggedIn, function(req,res){
    async.parallel({
      totCat : function(asyncCallback){
        dal.categoria.GetAll(asyncCallback);
      },
      totUser : function(asyncCallback){
        dal.users.GetDistinct(req.session.passport.user, asyncCallback);
      },
      artAut : function(asyncCallback){
        dal.users.GetByArticuloID(req.params.id, asyncCallback);
      },
      artCat : function(asyncCallback){
        dal.categoria.GetByArticuloID(req.params.id, asyncCallback);
      },
      articulo : function(asyncCallback){
        dal.articulos.GetByIdAndUserId(req.params.id, req.session.passport.user, asyncCallback);
      }
    },function(err, results){
      if(err || results.articulo.length==0){
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        res.redirect("/my-account/articulos");
      }
      else{
        var artCategorias = new Array();
        var artAutores = new Array();
        results.artCat.forEach(function(categoria){artCategorias.push(categoria.ID)});
        results.artAut.forEach(function(autor){artAutores.push(autor.ID)});
        res.render("profile/articulos/edit",{categorias: results.totCat, users: results.totUser, artAutores: artAutores, artCategorias: artCategorias, art:results.articulo[0]});
      }
    });
  });

  app.put("/my-account/articulos/:id", authHelper.isLoggedIn, function(req,res){
    async.series([
      function(callback){
        dal.articulos.GetByIdAndUserId(req.params.id, req.session.passport.user,function(err, rows){
          if(err )
            callback(err, null);
          else
            if (rows.length===0)
              callback("Este articulo no te pertenece", null);
            else
              callback(null, "COMPROBACIÓN");
        });
      },
      function(callback){
        if(req.body.publish)
          dal.articulos.Publish(req.params.id, req.body.publish, callback);
        else{
          var articulo = setArticulo(req.body.articulo, req.session.passport.user)
          articulo.id = req.params.id;
          dal.articulos.Update(articulo, callback);
        }
      }
    ], function(err, result){
      if(err){
        req.flash('error', 'Ha habido un error, por favor intentelo de nuevo');
        console.log(err);
      }
      else
        req.flash('correct', 'Se ha actualizado el artículo correctamente');
      res.redirect("/my-account/articulos");
    });
  });
}


function setArticulo (articulo, userId){
  if(articulo.subtitulo=="")
    articulo.subtitulo = null;
  if(articulo.descripcion=="")
    articulo.descripcion = null;
  articulo.fecha = new Date();
  articulo.users = setAutores(articulo.users, userId)
  articulo.categorias = setCategorias(articulo.categorias);
  return articulo;
}

function setAutores(users, userId){
  var autores = new Array();
  if(users){
    if(typeof(users) !='string')
      for(var i=0;i<users.length;i++){autores.push(Number(users[i]));}
    else
      autores.push(Number(users));
  }
  autores.push(userId);
  return autores;
}

function setCategorias(categorias){
  var newCategorias = new Array();
  if(categorias){
    if(typeof(categorias) != 'string')
      for(var i = 0  ; i<categorias.length ;i++ ) {newCategorias.push( Number(categorias[i]) );}
    else
      newCategorias.push(Number(categorias));
  }
  return newCategorias;
}
